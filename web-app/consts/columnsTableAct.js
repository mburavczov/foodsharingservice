export const COLUMNS_TABLE_ACT = [
    {
        title: '№',
        dataIndex: 'number',
        key: 'number',
    },
    {
        title: 'Название продукта',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Количество/вес',
        dataIndex: 'count',
        key: 'count',
    },
    {
        title: 'Стоимость (руб/шт.)',
        dataIndex: 'price',
        key: 'price',
    },
    {
        title: 'Срок годности',
        dataIndex: 'expire',
        key: 'expire',
    },
    {
        title: 'Примечания',
        dataIndex: 'comment',
        key: 'comment',
    }
]
