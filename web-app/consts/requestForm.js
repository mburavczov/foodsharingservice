import {Input, Switch} from "antd";

export const REQUEST_FORM_INPUT = [
    {
        name: 'login',
        label: 'E-mail',
        rules: [
            {
                type: 'email',
                message: 'Введен некорректный E-mail',
            },
            {
                required: true,
                message: 'Заполните поле "E-mail"',
            }
        ],
        children: <Input placeholder='Введите Ваш E-mail'/>
    },
    {
        name: 'name',
        label: 'Имя',
        rules: [
            {
                required: true,
                message: 'Заполните поле "Имя"',
            }
        ],
        children: <Input placeholder='Введите Ваше имя'/>
    },
    {
        name: 'surname',
        label: 'Фамилия',
        rules: [
            {
                required: true,
                message: 'Заполните поле "Фамилия"',
            }
        ],
        children: <Input placeholder='Введите Вашу фамилию'/>
    },
    {
        name: 'city',
        label: 'Город',
        rules: [
            {
                required: true,
                message: 'Заполните поле "Город"',
            }
        ],
        children: <Input placeholder='Введите Ваш город'/>
    },
    {
        name: 'address',
        label: 'Адрес',
        rules: [
            {
                required: true,
                message: 'Заполните поле "Адрес"',
            }
        ],
        children: <Input placeholder='Введите Ваш адрес'/>
    },
    {
        name: 'vk',
        label: 'Ссылка на страницу \'ВКонтакте\'',
        rules: [
            {
                required: true,
                message: 'Заполните поле cсылки на страницу ВКонтакте',
            }
        ],
        children: <Input placeholder='Введите cсылку на страницу ВКонтакте'/>
    },
    {
        name: 'phone',
        label: 'Номер телефона',
        rules: [],
        children: <Input placeholder='Введите Ваш номер телефона'/>
    },
    {
        name: 'comment',
        label: 'Комментарий',
        rules: [],
        children: <Input.TextArea autoSize={true} placeholder='Введите Ваш комментарий' />
    },
    {
        name: 'hasCar',
        label: 'Есть ли у Вас личный автомобиль?',
        rules: [],
        children: <Switch style={{margin: '-13px 0 0 10px'}}/>,
        style: {
            flexDirection: 'row',
        }
    }
]

