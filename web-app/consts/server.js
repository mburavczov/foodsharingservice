export const BACKEND_URL = 'https://foodsharing-api.herokuapp.com'
export const BACKEND_ACCOUNT_TOKEN_URL = `${BACKEND_URL}/api/account/token`
export const BACKEND_ACCOUNT_REGISTER = `${BACKEND_URL}/api/account/register`
export const BACKEND_NEW_REQUESTS = `${BACKEND_URL}/api/account/requests`
export const BACKEND_ACTIVATE_USER = `${BACKEND_URL}/api/account/ActivateUser`
export const BACKEND_ADD_GROUP = `${BACKEND_URL}/api/Group/Create`
export const BACKEND_ADD_USER_TO_GROUP = `${BACKEND_URL}/api/Group/AddUser`
export const BACKEND_GET_GROUPS = `${BACKEND_URL}/api/Group/Groups`
export const BACKEND_FIND_USERS = `${BACKEND_URL}/api/Person/Find`
export const BACKEND_GET_GROUP = (id) => `${BACKEND_GET_GROUPS}/${id}`
export const BACKEND_FILE_UPLOAD = `${BACKEND_URL}/api/File/Upload`
export const BACKEND_USER_CHANGE_STATE = `${BACKEND_URL}/api/Person/ChangeState`
export const BACKEND_GET_FILES = `${BACKEND_URL}/api/File/GetAllFiles`
export const BACKEND_GET_FILES_FOR_GROUP = `${BACKEND_URL}/api/File/GetFiles`
export const BACKEND_SCHEDULE_ADD = `${BACKEND_URL}/api/Schedule/Add`
export const BACKEND_SCHEDULE_DELETE = `${BACKEND_URL}/api/Schedule/Delete`
export const BACKEND_SCHEDULE_GET_FOR_GROUP = `${BACKEND_URL}/api/Schedule/GetForGroup`