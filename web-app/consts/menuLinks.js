import {ADMIN, SUPERVISOR, VOLONTER} from "./groupsUsers";
import {
    BarChartOutlined,
    HomeOutlined,
    TeamOutlined,
    GroupOutlined,
    PlusCircleOutlined,
    AuditOutlined,
    UserAddOutlined
} from "@ant-design/icons";

export const MENU_LINKS = [
    {
        title: 'Главная',
        icon: <HomeOutlined />,
        href: '/',
        access: [ADMIN, SUPERVISOR, VOLONTER],
        sort: 100
    },
    {
        title: 'Новые заявки',
        icon: <UserAddOutlined />,
        href: '/admin',
        access: [ADMIN],
        sort: 120
    },
    {
        title: 'Статистика',
        icon: <BarChartOutlined />,
        href: '/admin/stats',
        access: [ADMIN],
        sort: 140
    },
    {
        title: 'Моя группа',
        icon: <GroupOutlined />,
        href: '/groups/[id]',
        access: [SUPERVISOR, VOLONTER],
        sort: 400
    },
    {
        title: 'Добавить акт',
        icon: <PlusCircleOutlined />,
        href: '/add',
        access: [VOLONTER],
        sort: 800
    },
    {
        title: 'Акты',
        icon: <AuditOutlined />,
        href: '/acts',
        access: [ADMIN, SUPERVISOR],
        sort: 700
    },
    {
        title: 'Пользователи',
        icon: <TeamOutlined />,
        href: '/users',
        access: [ADMIN],
        sort: 130
    },
    {
        title: 'Группы',
        icon: <GroupOutlined />,
        href: '/groups',
        access: [ADMIN],
        sort: 200
    }
]
