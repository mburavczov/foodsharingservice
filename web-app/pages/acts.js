import AccessDenied from "../components/layout/AccessDenied";
import ActsList from "../components/acts/ActsList";
import {getSession} from "next-auth/client";
import {getAuth} from "../utils/getAuth";
import {ADMIN, SUPERVISOR} from "../consts/groupsUsers";
import axios from "axios";
import {BACKEND_GET_FILES, BACKEND_GET_FILES_FOR_GROUP} from "../consts/server";

const Acts = ({session, loader, acts}) => {
    loader.Effect()

    if (session && (session.role === ADMIN || session.role === SUPERVISOR)) {
        return (
            <ActsList acts={acts}/>
        );
    }

    return (
        <AccessDenied/>
    );
};

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    const auth = getAuth(session)
    if (session && (session.role === ADMIN || session.role === SUPERVISOR)) {
        switch (session.role) {
            case ADMIN:
                const adminActs = await axios.get(BACKEND_GET_FILES, {headers: {...auth}})
                return {props: {acts: adminActs.data}}
            case SUPERVISOR:
                const acts = await axios.get(BACKEND_GET_FILES_FOR_GROUP, {headers: {...auth}, params: {
                        groupId: session.groupId
                    }})
                return {props: {acts: acts.data}}
            default:
                return {props: {acts: []}}
        }
    } else {
        return {props: {acts: []}}
    }
}


export default Acts;