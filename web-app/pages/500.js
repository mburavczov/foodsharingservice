import Link from "next/link";
import {Button, Result} from "antd";

const Error = ({loader}) => {
    loader.Effect()

    return (
        <div className="container-page">
            <Result
                status="500"
                title="500"
                subTitle="Проблема с сервером."
                extra={
                    <Button onClick={() => loader.start()} type="primary" key="console">
                        <Link href='/'>Главная</Link>
                    </Button>
                }
            />
        </div>
    );
};

export default Error;