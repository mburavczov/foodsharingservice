import '../styles/globals.css'
import {getSession, Provider} from "next-auth/client"
import CrmMain from "../components/layout/CrmMain";
import {Spin} from "antd";
import {useEffect, useState} from "react";

export default function App({ Component, pageProps }) {
    const [isLoader, setIsLoader] = useState(false)
    const loader = {
        start: () => setIsLoader(true),
        end: () => setIsLoader(false),
        Effect: () => {
            useEffect(() => {
                setIsLoader(false)
            }, [])
        }
    }
    pageProps.loader = loader

    if (pageProps.session) {
        return (
            <Spin
                style={{zIndex: 1000}}
                spinning={isLoader}
                size="large"
            >
                <Provider session={pageProps}>
                    <CrmMain session={pageProps.session} loader={loader}>
                        <Component {...pageProps} />
                    </CrmMain>
                </Provider>
            </Spin>
        )
    }
    return (
        <Spin
            spinning={isLoader}
            size="large"
        >
            <Component {...pageProps} />
        </Spin>
    )
}

App.getInitialProps = async (ctx) => {
    return {
        pageProps: {
            session: await getSession(ctx)
        }
    }
}