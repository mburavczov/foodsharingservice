import {Result, Button} from "antd";
import Link from "next/link";

const Error = ({loader}) => {
    loader.Effect()

    return (
        <div className="container-page">
            <Result
                status="404"
                title="404"
                subTitle="Страница не найдена."
                extra={
                    <Button onClick={() => loader.start()} type="primary" key="console">
                        <Link href='/'>Главная</Link>
                    </Button>
                }
            />
        </div>
    );
};

export default Error;