import {getCsrfToken} from "next-auth/client";
import AuthForm from "../../components/forms/AuthForm";
import axios from "axios";
import {Button, Result} from "antd";
import Link from "next/link";
import {useState} from "react";
import {useRouter} from "next/router";

const Auth = ({csrfToken, loader, session}) => {
    loader.Effect()
    const router = useRouter()
    const [error, setError] = useState(false)

    const onFinish = async (body) => {
        try {
            loader.start()
            await axios.post('/api/auth/callback/credentials', body)
            router.push('/')
        } catch {
            setError(true)
            loader.end()
        }
    }

    if(session) {
        return (
            <div className='container-page'>
                <Result
                    title="Вы уже авторизованы."
                    extra={
                        <Button type="primary" key="console">
                            <Link href='/'>Главная</Link>
                        </Button>
                    }
                />
            </div>
        )
    }

    return (
        <div className='container-page'>
            <AuthForm csrfToken={csrfToken} onFinish={onFinish} error={error}/>
        </div>
    );
};

export async function getServerSideProps(context) {
    return {
        props: {
            csrfToken: await getCsrfToken(context)
        }
    }
}

export default Auth;