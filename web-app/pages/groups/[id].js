import AccessDenied from "../../components/layout/AccessDenied";
import {getSession} from "next-auth/client";
import {getAuth} from "../../utils/getAuth";
import TheGroup from "../../components/groups/TheGroup";
import {ADMIN} from "../../consts/groupsUsers";
import {getDataToTheGroup} from "../../utils/getDataToTheGroup";
import {useRouter} from "next/router";

const Group = ({session, loader, dataToGroup}) => {
    loader.Effect()
    const router = useRouter()

    if (session && (session.role === ADMIN || session.groupId === router.query.id)) {
        return (
            <TheGroup
                dataToGroup={dataToGroup}
                loader={loader}
                session={session}
            />
        );
    }

    return (
        <AccessDenied/>
    );
};

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    const auth = getAuth(session)
    if (session && (session.role === ADMIN || session.groupId === ctx.query.id)) {
        return {
            props: {
                dataToGroup: await getDataToTheGroup(auth, ctx.query.id, session.role)
            }
        }
    } else {
        return {props: {dataToGroup: {group: [], volonters: []}}}
    }
}

export default Group;