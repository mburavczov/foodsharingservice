import AccessDenied from "../../components/layout/AccessDenied";
import {ADMIN} from "../../consts/groupsUsers";
import {getSession} from "next-auth/client";
import {getAuth} from "../../utils/getAuth";
import GroupsMain from "../../components/groups/GroupsMain";
import {getDataToGroups} from "../../utils/getDataToGroups";

const Groups = ({session, loader, dataToGroups}) => {
    loader.Effect()

    if (session && session.role === ADMIN) {
        const auth = getAuth(session)
        return (
            <GroupsMain
                dataToGroups={dataToGroups}
                loader={loader}
                auth={auth}
                role={session.role}
            />
        );
    }

    return (
        <AccessDenied/>
    );
};

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    const auth = getAuth(session)
    if (session && session.role === ADMIN) {
        return {
            props: {
                dataToGroups: await getDataToGroups(auth)
            }
        }
    } else {
        return {props: {dataToGroups: {groups: [], supervisors: [], volonters: []}}}
    }
}

export default Groups;