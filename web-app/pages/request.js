import RequestForm from "../components/forms/RequestForm";

const Request = ({session, loader}) => {
    loader.Effect()

    if (session) {
        return (
            <h1>Вы уже являетесь партнером!</h1>
        )
    }

    return (
        <RequestForm loader={loader}/>
    );
};

export default Request;