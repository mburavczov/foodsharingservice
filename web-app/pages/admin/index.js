import {ADMIN} from "../../consts/groupsUsers";
import AccessDenied from "../../components/layout/AccessDenied";
import NewRequests from "../../components/admin/NewRequests";
import axios from "axios";
import {BACKEND_NEW_REQUESTS} from "../../consts/server";
import {useState} from "react";
import {getAuth} from "../../utils/getAuth";
import {getSession} from "next-auth/client";

const Admin = ({session, loader, newRequests}) => {
    const [requests, setRequests] = useState(newRequests)
    const auth = getAuth(session)

    loader.Effect()

    if (session && session.role === ADMIN) {
        return (
            <NewRequests requests={requests} setRequests={setRequests} auth={auth} loader={loader}/>
        );
    }

    return (
        <AccessDenied/>
    );
};

export async function getServerSideProps(ctx) {
    const session = await getSession(ctx)
    const auth = getAuth(session)
    if (session && session.role === ADMIN) {
        const newRequests = await axios.get(BACKEND_NEW_REQUESTS, {headers: {...auth}})
        return {props: {newRequests: newRequests.data}}
    } else {
        return {props: {newRequests: []}}
    }
}

export default Admin