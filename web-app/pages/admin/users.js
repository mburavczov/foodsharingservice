import AccessDenied from "../../components/layout/AccessDenied";
import {ADMIN} from "../../consts/groupsUsers";
import InDeveloper from "../../components/layout/InDeveloper";

const Users = ({session, loader}) => {
    loader.Effect()

    if (session && (session.role === ADMIN)) {
        return (
            <InDeveloper />
        );
    }

    return (
        <AccessDenied/>
    );
};

export default Users;