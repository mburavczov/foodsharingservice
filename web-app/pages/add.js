import AccessDenied from "../components/layout/AccessDenied";
import AddAct from "../components/other/AddAct";

const AddActPage = ({session, loader}) => {
    loader.Effect()

    if (session) {
        return (
            <AddAct session={session} loader={loader}/>
        );
    }

    return (
        <AccessDenied />
    );
};

export default AddActPage;