import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import axios from "axios";
import jwtDecode from "jwt-decode";
import {BACKEND_ACCOUNT_TOKEN_URL} from "../../../consts/server";

const options = {
    site: process.env.NEXTAUTH_URL,
    providers : [
        Providers.Credentials({
            name: 'Foodshering',
            credentials: {
                login: { label: 'Email', type: 'text', placeholder: 'Введите Email' },
                pass: { label: 'Пароль', type: 'password', placeholder: 'Введите пароль'  }
            },
            async authorize(credentials) {
                const response = await axios.post(BACKEND_ACCOUNT_TOKEN_URL, credentials)

                if(response) {
                    return response.data
                }

                return null
            }
        })
    ],
    callbacks: {
        async jwt(token, user) {
            if (user) {
                token = {
                    ...jwtDecode(user.access_token),
                    accessToken: user.access_token
                }
            }

            return token;
        },
        async session(session, token) {
            return token
        }
    },
    pages: {
        signIn: '/auth',
        error: '/auth/error'
    }
}

const result = (req, res) => NextAuth(req, res, options)
export default result