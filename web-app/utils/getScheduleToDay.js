import moment from "moment";
import {
    TYPE_SCHEDULE_ITEM_EXCLUDE,
    TYPE_SCHEDULE_ITEM_INCLUDE,
    TYPE_SCHEDULE_ITEM_PERIODIC
} from "../consts/typesScheduleItem";

export const getScheduleToDay = (value, schedule) => {
    let list = []

    schedule.filter(item => item.type === TYPE_SCHEDULE_ITEM_PERIODIC).forEach(item => {
        if(item.dayOfWeek === value.day())
            list.push(addItemForList(item))
    })

    schedule.filter(item => item.type === TYPE_SCHEDULE_ITEM_INCLUDE).forEach(item => {
        const momentDateAsItem = moment(item.date)
        if(momentDateAsItem.format('ll') === value.format('ll'))
            list.push(addItemForList(item))
    })

    schedule.filter(item => item.type === TYPE_SCHEDULE_ITEM_EXCLUDE).forEach(item => {
        const momentDateAsItem = moment(item.date)
        if(momentDateAsItem.format('ll') === value.format('ll')) {
            list.forEach((elem, i, elems) => {
                if(elem.userId === item.userId) {
                    elems[i] = {...elem, type: 'error'}
                }
            })
        }
    })

    return list
}

const addItemForList = (item) => ({
    type: 'success',
    content: `${item.user.name} ${item.user.surname}`,
    userId: item.userId
})