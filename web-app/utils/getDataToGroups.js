import axios from "axios";
import {BACKEND_FIND_USERS, BACKEND_GET_GROUPS} from "../consts/server";
import {SUPERVISOR, VOLONTER} from "../consts/groupsUsers";

export const getDataToGroups = async (auth) => {
    const [groups, supervisors, volonters] = await Promise.all([
        axios.get(BACKEND_GET_GROUPS, {headers: {...auth}}),
        axios.get(BACKEND_FIND_USERS, {headers: {...auth}, params: {
                HasGroup: false,
                Role: SUPERVISOR
            }}),
        axios.get(BACKEND_FIND_USERS, {headers: {...auth}, params: {
                HasGroup: false,
                Role: VOLONTER
            }})
    ])

    return {
        groups: groups.data,
        supervisors: supervisors.data,
        volonters: volonters.data
    }
}