import {MENU_LINKS} from "../consts/menuLinks";

export const getMenuItems = (role, id = null) => {
    return  MENU_LINKS
        .filter(item => item.access.includes(role))
        .sort((a, b) => a.sort - b.sort)
        .map(item => {
            if(item.href.includes('[id]')) {
                item.href = item.href.replace('\[id]', id)
            }
            return item
        })
}