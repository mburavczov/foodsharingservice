export const getAuth = (session) => {
    if (session && session.accessToken) {
        return {
            Authorization: `Bearer ${session.accessToken}`
        }
    }
    return false
};