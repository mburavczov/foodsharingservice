import axios from "axios";
import {
    BACKEND_FIND_USERS,
    BACKEND_GET_GROUP,
    BACKEND_SCHEDULE_GET_FOR_GROUP
} from "../consts/server";
import {VOLONTER} from "../consts/groupsUsers";

export const getDataToTheGroup = async (auth, groupId, role) => {
    if(role === VOLONTER) {
        const [group, schedule] = await Promise.all([
            axios.get(BACKEND_GET_GROUP(groupId), {headers: {...auth}}),
            axios.get(BACKEND_SCHEDULE_GET_FOR_GROUP, {headers: {...auth}, params: {groupId}})
        ])
        return {
            schedule: schedule.data,
            group: group.data,
            volonters: []
        }
    }

    const [group, schedule, volonters] = await Promise.all([
        axios.get(BACKEND_GET_GROUP(groupId), {headers: {...auth}}),
        axios.get(BACKEND_SCHEDULE_GET_FOR_GROUP, {headers: {...auth}, params: {groupId}}),
        axios.get(BACKEND_FIND_USERS, {headers: {...auth}, params: {
            HasGroup: false,
            Role: VOLONTER
        }})
    ])

    return {
        group: group.data,
        schedule: schedule.data,
        volonters: volonters.data
    }
}