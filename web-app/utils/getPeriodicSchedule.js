import {VOLONTER} from "../consts/groupsUsers";
import {TYPE_SCHEDULE_ITEM_PERIODIC} from "../consts/typesScheduleItem";

export const getPeriodicSchedule = (schedule, role, userId) => {
    let periodicSchedule
    if (role === VOLONTER) {
        periodicSchedule = schedule.filter(item => item.type === TYPE_SCHEDULE_ITEM_PERIODIC && item.userId === Number(userId))
    } else {
        periodicSchedule = schedule.filter(item => item.type === TYPE_SCHEDULE_ITEM_PERIODIC)
    }

    periodicSchedule = periodicSchedule.sort((a, b) => {
        let aDay = a.dayOfWeek, bDay = b.dayOfWeek
        if(aDay === 0) aDay = 7
        if(bDay === 0) bDay = 7
        return aDay - bDay
    })

    return periodicSchedule
}