import {Menu} from "antd";
import Link from "next/link";
import {signOut} from "next-auth/client";
import React from "react";
import {ImportOutlined} from "@ant-design/icons";

const Sidebar = ({items, path, loader}) => {
    return (
        <Menu
            mode="inline"
            selectedKeys={path}
            style={{height: '100%', borderRight: 0}}
        >
            {items.map(item => {
                return (
                    <Menu.Item key={item.href} icon={item.icon}>
                        <Link
                            href={item.href}
                        >
                            <a onClick={() => loader.start()}>{item.title}</a>
                        </Link>
                    </Menu.Item>
                )
            })}
            <Menu.Item key={'Выйти'} onClick={() => signOut({ callbackUrl: '/' })} icon={<ImportOutlined />}>
                <a>{'Выйти'}</a>
            </Menu.Item>
        </Menu>
    )
};

export default Sidebar;