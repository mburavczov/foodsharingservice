import { Result, Button } from 'antd';
import Link from "next/link";

const AccessDenied = () => {
    return (
        <div className="container-page">
            <Result
                status="warning"
                title="Доступ запрещен."
                extra={
                    <Button type="primary" key="console">
                        <Link href='/'>Главная</Link>
                    </Button>
                }
            />
        </div>
    );
};

export default AccessDenied;