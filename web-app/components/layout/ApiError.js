import {Result} from "antd";

const ApiError = () => {
    return (
        <div className="container-page">
            <Result
                status="error"
                title="При отправке данных произошла ошибка."
                subTitle="Попробуйте обратиться к сервису позже."
            />
        </div>
    );
};

export default ApiError;