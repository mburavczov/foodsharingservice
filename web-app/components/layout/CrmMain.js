import {Button, Dropdown, Layout, Typography} from 'antd';
import Sidebar from "./Sidebar";

const {Header, Content} = Layout;
const {Title} = Typography

import styles from '../../styles/components/layout/CrmMain.module.css'
import {getMenuItems} from "../../utils/getMenuItems";
import {DownOutlined} from "@ant-design/icons";
import {useRouter} from "next/router";

const CrmMain = ({children, session, loader}) => {
    const menuItems = getMenuItems(session.role, session.groupId)
    const router = useRouter()
    const path = router.asPath
    const dropdownTitle = menuItems.find(item => item.href === path ? item : undefined )

    return (
        <Layout>
            <Header className={styles.header}>
                <Title
                    type={'warning'}
                    className={styles.headerTitle}
                >
                    АНО &quot;ФУДШЕРИНГ&quot;
                </Title>
            </Header>
            <Layout className={styles.container}>
                <Layout.Sider
                    width={200}
                    className={[styles.sider, 'site-layout-background']}
                >
                    <Sidebar items={menuItems} path={path} loader={loader}/>
                </Layout.Sider>
                <Layout style={{padding: '0 24px 24px'}}>
                    <Dropdown
                        overlay={<Sidebar items={menuItems} path={path}/>}
                        className={styles.dropdown}
                        trigger={['click']}
                    >
                        <Button>
                            {dropdownTitle ? dropdownTitle.title : 'Меню' } <DownOutlined />
                        </Button>
                    </Dropdown>
                    <Content
                        className={[styles.content, 'site-layout-background']}
                    >
                        {children}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};

export default CrmMain;