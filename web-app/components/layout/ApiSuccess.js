import {Result} from "antd";

const ApiSuccess = () => {
    return (
        <div className="container-page">
            <Result
                status="success"
                title="Данные успешно отправлены."
            />
        </div>
    );
};

export default ApiSuccess;