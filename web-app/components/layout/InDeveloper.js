import {Button, Result} from "antd";
import Link from "next/link";

const InDeveloper = () => {
    return (
        <div className="container-page">
            <Result
                title="Страница находится в разработке."
                extra={
                    <Button type="primary" key="console">
                        <Link href='/'>Главная</Link>
                    </Button>
                }
            />
        </div>
    );
};

export default InDeveloper;