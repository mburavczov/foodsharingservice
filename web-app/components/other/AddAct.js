import {Typography} from "antd";
import AddActForm from "../forms/AddActForm";
import {getAuth} from "../../utils/getAuth";

const AddAct = ({session, loader}) => {
    return (
        <>
            <Typography.Title level={4}>Добавление акта</Typography.Title>
            <AddActForm
                loader={loader}
                auth={getAuth(session)} userId={session.user_id}
            />
        </>
    );
};

export default AddAct;