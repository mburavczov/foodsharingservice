import {CaretRightOutlined} from "@ant-design/icons";
import {Button, Collapse, Col, Row} from "antd";
import VolonterList from "./VolonterList";
import AddUserToGroup from "../../forms/AddUserToGroup";
import Link from "next/link";

const GroupsList = ({groups, userAddToGroup, volonters, loader, role, auth, updateMain}) => {
    if (groups.length === 0) {
        return (
            <h2>В данный момент не создано ни одной группы.</h2>
        )
    }

    return (
        <Collapse
            style={{marginTop: '20px'}}
            bordered={false}
            expandIcon={({isActive}) => <CaretRightOutlined rotate={isActive ? 90 : 0}/>}
            className="site-collapse-custom-collapse"
        >
            {
                groups.map(item => {
                    return (
                        <Collapse.Panel
                            header={item.name}
                            key={item.id}
                            className={'site-collapse-custom-panel'}
                        >
                            <Row gutter={[16, 16]}>
                                <Col>
                                    <p>Описание: {item.description}</p>
                                    <p>Координатор: {item.supervisorName}</p>
                                    <VolonterList role={role} users={item.users} auth={auth} loader={loader} postBlock={updateMain}/>
                                    <Button
                                        onClick={() => loader.start()}
                                        type="primary"
                                        style={{marginTop: '10px'}}
                                    >
                                        <Link href={`/groups/${item.id}`}>
                                            Расписание
                                        </Link>
                                    </Button>
                                </Col>
                                <Col>
                                    <AddUserToGroup groupId={item.id} userAddToGroup={userAddToGroup} users={volonters}/>
                                </Col>
                            </Row>
                        </Collapse.Panel>
                    )
                })
            }
        </Collapse>
    );
};

export default GroupsList;