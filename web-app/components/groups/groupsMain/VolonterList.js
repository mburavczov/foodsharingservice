import {ADMIN, SUPERVISOR, VOLONTER} from "../../../consts/groupsUsers";
import BlockUser from "../../forms/BlockUser";
import {Collapse} from "antd";

const VolonterList = ({users, role, loader, auth, postBlock = null}) => {
    const isBlockComponent = (role === ADMIN || role === SUPERVISOR)

    if(users.length === 0) {
        return (
            <span>В данной группе нет ни одного волонтера.</span>
        )
    }

    return (
        <Collapse style={{width: '200px'}}>
            <Collapse.Panel header={'Волонтеры'}>
                {users.filter(item => item.role === VOLONTER).map(item => (
                    <div key={item.id}>
                        {`${item.name} ${item.surname}`}
                        <div
                            style={{marginBottom: '10px'}}
                        >
                            {isBlockComponent ?
                                <BlockUser loader={loader} auth={auth} user={item} postBlock={postBlock}/>
                                : null}
                        </div>
                    </div>
                ))}
            </Collapse.Panel>
        </Collapse>
    );
};

export default VolonterList;