import {Button, Modal} from "antd";
import GroupCreateForm from "../../forms/GroupCreateForm";

const GroupCreate = ({isCreate, setIsCreate, supervisors, groupAdd}) => {
    if (supervisors.length === 0) {
        return (
            <>
                <Button
                    style={{margin: '10px 0'}}
                    type="primary"
                    disabled={true}
                >
                    Создать группу
                </Button>
                <span style={{marginLeft: '10px'}}>Нет свободных координаторов.</span>
            </>
        )
    }

    return (
        <>
            <Button
                style={{marginTop: '10px'}}
                onClick={() => setIsCreate(true)}
                type="primary"
            >
                Создать группу
            </Button>
            <Modal
                title="Создание группы"
                centered
                visible={isCreate}
                footer={[]}
                onCancel={() => setIsCreate(false)}
                zIndex={15}
            >
                <GroupCreateForm
                    groupAdd={groupAdd}
                    supervisors={supervisors}
                />
            </Modal>
        </>
    );
};

export default GroupCreate;