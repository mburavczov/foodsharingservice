import {Typography, Row, Col} from "antd";
import {useState} from "react";
import VolonterList from "./groupsMain/VolonterList";
import {getAuth} from "../../utils/getAuth";
import ApiError from "../layout/ApiError";
import {getDataToTheGroup} from "../../utils/getDataToTheGroup";
import {ADMIN, SUPERVISOR} from "../../consts/groupsUsers";
import AddUserToGroup from "../forms/AddUserToGroup";
import axios from "axios";
import {BACKEND_ADD_USER_TO_GROUP} from "../../consts/server";
import Schedule from "../schedule/Schedule";
import PeriodicScheduleUpdate from "../schedule/PeriodicScheduleUpdate";
import IncScheduleModal from "../schedule/IncScheduleModal";

const TheGroup = ({dataToGroup, loader, session}) => {
    const auth = getAuth(session)
    const [group, setGroup] = useState(dataToGroup.group)
    const [volonters, setVolonters] = useState(dataToGroup.volonters)
    const [schedule, setSchedule] = useState(dataToGroup.schedule)
    const [incSchedule, setIncSchedule] = useState(false)
    const [error, setError] = useState(false)

    if (error) {
        return (
            <ApiError/>
        )
    }

    const updateGroup = async () => {
        try {
            const data = await getDataToTheGroup(auth, group.id, session.role)
            setGroup(data.group)
            setVolonters(data.volonters)
            setSchedule(data.schedule)
            loader.end()
        } catch {
            setError(true)
            loader.end()
        }
    }

    let addUser = null
    if(session.role === ADMIN || session.role === SUPERVISOR){
        const userAddToGroup = async (body, form) => {
            loader.start()
            try {
                await axios.post(BACKEND_ADD_USER_TO_GROUP, body, {headers: {...auth}})
                await updateGroup()
                form.resetFields()
            } catch {
                setError(true)
                loader.end()
            }
        }
        addUser = (
            <AddUserToGroup groupId={group.id} userAddToGroup={userAddToGroup} users={volonters}/>
        )
    }

    const updateSchedule = async (body, url) => {
        loader.start()
        try {
            await axios.post(url, body, {headers: {...auth}})
            await updateGroup()
        } catch {
            setError(true)
            loader.end()
        }
    }

    return (
        <>
            <Typography.Title level={4}>{`Группа #${group.id}: ${group.name}`}</Typography.Title>
            <Row>
                <Col lg={{span:5}}>
                    <Typography.Paragraph >{`Описание: ${group.description}`}</Typography.Paragraph>
                    <Typography.Paragraph >{`Координатор: ${group.supervisorName}`}</Typography.Paragraph>
                    <VolonterList
                        users={group.users}
                        role={session.role}
                        loader={loader}
                        auth={auth}
                        postBlock={updateGroup}
                    />
                    <div style={{height: '10px'}}/>
                    {addUser}
                    <PeriodicScheduleUpdate
                        schedule={schedule}
                        updateFunc={updateSchedule}
                        userId={session.user_id}
                        role={session.role}
                        groupUsers={group.users}
                    />
                </Col>
                <Col lg={{span:19}}>
                    <Schedule
                        schedule={schedule}
                        setIncSchedule={setIncSchedule}
                    />
                    <IncScheduleModal
                        schedule={schedule}
                        updateFunc={updateSchedule}
                        userId={session.user_id}
                        role={session.role}
                        groupUsers={group.users}
                        incSchedule={incSchedule}
                        setIncSchedule={setIncSchedule}
                    />
                </Col>
            </Row>
        </>
    );
};

export default TheGroup;