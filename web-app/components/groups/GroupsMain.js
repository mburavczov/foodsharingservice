import GroupCreate from "./groupsMain/GroupCreate";
import GroupsList from "./groupsMain/GroupsList";
import axios from "axios";
import {BACKEND_ADD_GROUP, BACKEND_ADD_USER_TO_GROUP} from "../../consts/server";
import {useState} from "react";
import ApiError from "../layout/ApiError";
import {getDataToGroups} from "../../utils/getDataToGroups";

const GroupsMain = ({dataToGroups, loader, auth, role}) => {
    const [groups, setGroups] = useState(dataToGroups.groups)
    const [supervisors, setSupervisors] = useState(dataToGroups.supervisors)
    const [volonters, setVolonters] = useState(dataToGroups.volonters)
    const [isCreate, setIsCreate] = useState(false)
    const [error, setError] = useState(false)

    if(error) {
        return (
            <ApiError />
        )
    }

    const updateMain = async () => {
        try {
            const data = await getDataToGroups(auth)
            setGroups(data.groups)
            setSupervisors(data.supervisors)
            setVolonters(data.volonters)
            loader.end()
        } catch {
            setError(true)
            loader.end()
        }
    }

    const groupAdd = async (body) => {
        loader.start()
        try {
            await axios.post(BACKEND_ADD_GROUP, body, {headers: {...auth}})
            await updateMain()
            setIsCreate(false)
        } catch {
            setError(true)
            loader.end()
        }
    }

    const userAddToGroup = async (body, form) => {
        loader.start()
        try {
            await axios.post(BACKEND_ADD_USER_TO_GROUP, body, {headers: {...auth}})
            await updateMain()
            form.resetFields()
        } catch {
            setError(true)
            loader.end()
        }
    }

    return (
        <>
            <h1>Группы волонтёров</h1>
            <GroupCreate
                supervisors={supervisors}
                groupAdd={groupAdd}
                isCreate={isCreate}
                setIsCreate={setIsCreate}
            />
            <GroupsList
                updateMain={updateMain}
                auth={auth}
                role={role}
                loader={loader}
                groups={groups}
                volonters={volonters}
                userAddToGroup={userAddToGroup}
            />
        </>
    );
};

export default GroupsMain;