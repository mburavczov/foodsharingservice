import {Button, Collapse} from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';
import {useState} from "react";
import axios from "axios";
import {BACKEND_ACTIVATE_USER} from "../../consts/server";
import ApiError from "../layout/ApiError";

const { Panel } = Collapse;

const NewRequests = ({requests, setRequests, auth, loader}) => {
    const [error, setError] = useState(false)
    if(error) {
        return (
            <ApiError />
        )
    }

    const add = async (id) => {
        try {
            loader.start()
            await axios.post(BACKEND_ACTIVATE_USER, {userId: id}, {headers: {...auth}})
            setRequests(prev => prev.filter(item => item.id != id))
            loader.end()
        } catch {
            setError(true)
            loader.end()
        }
    }

    if(requests.length === 0) {
        return (
            <div>
                <h1>Новых заявок в данный момент нет. </h1>
            </div>
        )
    }

    return (
        <div>
            <h1>Новые заявки на участие в проекте.</h1>
            <Collapse
                style={{marginTop: '10px'}}
                bordered={false}
                expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                className="site-collapse-custom-collapse"
            >
                {
                    requests.map(item => {
                        return (
                            <Panel
                                header={`${item.name} ${item.surname}`}
                                key={item.id}
                                className="site-collapse-custom-panel"
                            >
                                <ul>
                                    <li>Электронная почта: {item.login}</li>
                                    <li>Город: {item.city}</li>
                                    <li>Адрес: {item.address}</li>
                                    <li><a rel="noreferrer" target='_blank' href={item.vk}>{item.vk}</a></li>
                                    <li>Телефон: {item.phone}</li>
                                    <li>Комментарий: {item.comment}</li>
                                    <li>{item.hasCar ? 'Имеется личный автомобиль' : 'Нет личного автомобиля'}</li>
                                    <Button
                                        style={{marginTop: '10px'}}
                                        type="primary"
                                        onClick={() => add(item.id)}
                                    >
                                        Одобрить
                                    </Button>
                                </ul>
                            </Panel>
                        )
                    })
                }
            </Collapse>
        </div>
    );
};

export default NewRequests;