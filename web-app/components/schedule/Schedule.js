import {Badge, Calendar} from "antd";
import {LOCALE_CALENDAR} from "../../consts/localeCalendar";
import moment from "moment";
import 'moment/locale/ru'
import {getScheduleToDay} from "../../utils/getScheduleToDay";
moment.locale('ru')

const Schedule = ({schedule, setIncSchedule}) => {
    const dateCellRender = (value) => {
        const listData = getScheduleToDay(value, schedule);

        if(listData.length === 0) {
            return null
        }

        return (
            <>
                {listData.map((item, i) => (
                    <Badge key={item.content + i} status={item.type} text={item.content} />
                ))}
            </>
        );
    }

    return (
        <Calendar
            style={{padding: '10px'}}
            locale={LOCALE_CALENDAR}
            dateCellRender={dateCellRender}
            onChange={(date) => setIncSchedule(date.toISOString())}
        />
    );
};

export default Schedule;