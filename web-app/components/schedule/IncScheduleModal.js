import {Button, Form, Input, Modal, Select} from "antd";
import {
    TYPE_SCHEDULE_ITEM_EXCLUDE,
    TYPE_SCHEDULE_ITEM_INCLUDE
} from "../../consts/typesScheduleItem";
import {VOLONTER} from "../../consts/groupsUsers";
import {BACKEND_SCHEDULE_ADD} from "../../consts/server";
import {ACTIVE_USER} from "../../consts/statesUsers";

const IncScheduleModal = ({updateFunc, userId, role, groupUsers, incSchedule, setIncSchedule}) => {
    const [form] = Form.useForm()

    const formItemDependOnRole = (role === VOLONTER) ?
        (
            <Form.Item
                name={'userId'}
                initialValue={userId}
                hidden={true}
            >
                <Input/>
            </Form.Item>
        ) :
        (
            <Form.Item
                style={{marginTop: '-10px'}}
                name={'userId'}
                rules={[{required: true, message: ''}]}
            >
                <Select
                    style={{width: '100%'}}
                    placeholder="Волонтёр"
                >
                    {groupUsers.filter(item => item.state === ACTIVE_USER).map(item => (
                        <Select.Option key={item.id} value={item.id}>
                            {item.name} {item.surname}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
        )

    return (
        <Modal
            title="Изменение расписания дня"
            centered
            visible={(incSchedule)}
            footer={[]}
            onCancel={() => setIncSchedule(false)}
        >
            <Form
                form={form}
                style={{width: '200px', margin: '30px auto'}}
                layout={'vertical'}
                name="addPeriodicSchedule"
                onFinish={(body) => {
                    body = {...body, date: incSchedule}
                    updateFunc(body, BACKEND_SCHEDULE_ADD)
                    setIncSchedule(false)
                    form.resetFields()
                }}
            >
                {formItemDependOnRole}
                <Form.Item
                    name={'type'}
                    rules={[{required: true, message: ''}]}
                >
                    <Select
                        style={{width: '100%'}}
                        placeholder="Действие"
                    >
                        <Select.Option key={'add'} value={TYPE_SCHEDULE_ITEM_INCLUDE}>Добавить</Select.Option>
                        <Select.Option key={'delete'} value={TYPE_SCHEDULE_ITEM_EXCLUDE}>Отменить</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item style={{marginTop: '-10px'}}>
                    <Button
                        style={{width: '100%'}}
                        type="primary"
                        htmlType="submit"
                    >
                        Выполнить
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default IncScheduleModal;