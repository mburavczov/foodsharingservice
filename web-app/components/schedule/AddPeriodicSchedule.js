import {Button, Form, Input, Select, Typography} from "antd";
import {NAMES_DAY_OF_WEEK} from "../../consts/namesDayOfWeek";
import {TYPE_SCHEDULE_ITEM_PERIODIC} from "../../consts/typesScheduleItem";
import {VOLONTER} from "../../consts/groupsUsers";
import {ACTIVE_USER} from "../../consts/statesUsers";

const AddPeriodicSchedule = ({role, userId, addRecordOfSchedule, groupUsers}) => {
    const [form] = Form.useForm()

    const formItemDependOnRole = (role === VOLONTER) ?
        (
            <Form.Item
                name={'userId'}
                initialValue={userId}
                hidden={true}
            >
                <Input/>
            </Form.Item>
        ) :
        (
            <Form.Item
                style={{marginTop: '-10px'}}
                name={'userId'}
                rules={[{required: true, message: ''}]}
            >
                <Select
                    style={{width: '100%'}}
                    placeholder="Волонтёр"
                >
                    {groupUsers.filter(item => item.state === ACTIVE_USER).map(item => (
                        <Select.Option key={item.id} value={item.id}>
                            {item.name} {item.surname}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
        )

    return (
        <>
            <Typography.Title
                level={5}
                style={{marginTop: '10px'}}
            >
                Дополнение расписания
            </Typography.Title>
            <Form
                form={form}
                style={{width: '200px'}}
                layout={'vertical'}
                name="addPeriodicSchedule"
                onFinish={(body) => {
                    addRecordOfSchedule(body)
                    form.resetFields()
                }}
            >
                <Form.Item
                    name={'type'}
                    initialValue={TYPE_SCHEDULE_ITEM_PERIODIC}
                    hidden={true}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    name={'dayOfWeek'}
                    rules={[{required: true, message: ''}]}
                >
                    <Select
                        style={{width: '100%'}}
                        placeholder="День недели"
                    >
                        {NAMES_DAY_OF_WEEK.map((item, i) => (
                            <Select.Option key={i} value={i}>{item}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                {formItemDependOnRole}
                <Form.Item style={{marginTop: '-10px'}}>
                    <Button
                        style={{width: '100%'}}
                        type="primary"
                        htmlType="submit"
                    >
                        Дополнить
                    </Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default AddPeriodicSchedule;