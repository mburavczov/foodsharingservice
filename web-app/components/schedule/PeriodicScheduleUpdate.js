import {Typography} from "antd";
import PeriodicSchedule from "./PeriodicSchedule";
import {getPeriodicSchedule} from "../../utils/getPeriodicSchedule";
import {BACKEND_SCHEDULE_ADD, BACKEND_SCHEDULE_DELETE} from "../../consts/server";
import AddPeriodicSchedule from "./AddPeriodicSchedule";

const PeriodicScheduleUpdate = ({schedule, updateFunc, userId, role, groupUsers}) => {
    const periodicSchedule = getPeriodicSchedule(schedule, role, userId)

    const deleteRecordOfSchedule = (id) => updateFunc({id}, BACKEND_SCHEDULE_DELETE)
    const addRecordOfSchedule = (body) => updateFunc(body, BACKEND_SCHEDULE_ADD)

    return (
        <>
            <Typography.Title
                level={5}
                style={{marginBottom: '10px'}}
            >
                Периодическое расписание
            </Typography.Title>
            <PeriodicSchedule
                schedule={periodicSchedule}
                deleteFunc={deleteRecordOfSchedule}
            />
            <AddPeriodicSchedule
                role={role}
                userId={userId}
                addRecordOfSchedule={addRecordOfSchedule}
                groupUsers={groupUsers}
            />
        </>
    );
};

export default PeriodicScheduleUpdate;