import {List} from "antd";
import {CloseOutlined} from "@ant-design/icons";
import {NAMES_DAY_OF_WEEK} from "../../consts/namesDayOfWeek";

const PeriodicScheduleUpdate = ({schedule, deleteFunc}) => {
    if(schedule.length === 0) {
        return (
            <h4>Пока нет записей в расписании</h4>
        );
    }

    const editedSchedule = schedule.map(item => {
        return {
            name: `${NAMES_DAY_OF_WEEK[item.dayOfWeek]} - ${item.user.name} ${item.user.surname}`,
            id: item.id
        }
    })

    return (
        <List
            dataSource={editedSchedule}
            renderItem={(item, i) => (
                <List.Item key={item.name + i} style={{padding: 0}}>
                    {item.name}
                    <CloseOutlined
                        className='delete-icon'
                        onClick={() => deleteFunc(item.id)}
                    />
                </List.Item>)
            }
        />
    );
};

export default PeriodicScheduleUpdate;