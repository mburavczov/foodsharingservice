import React from 'react';
import {signIn} from "next-auth/client";
import {Typography} from 'antd';
import {Button} from 'antd';
import styles from "../../styles/components/recruting/Recruting.module.css";
import Image from "next/image";
import logo from "../../public/logo.png";
import Link from "next/link";

const {Title} = Typography;

const Recruting = ({loader}) => {
    return (
        <div>
            <div className={styles.header}>
                <div className={styles.header__container}>
                    <nav className={styles.nav}>
                        <Image
                            className={styles.header__logo}
                            src={logo}
                            width={60}
                            height={60}
                            alt={'Фудшеринг'}
                        />
                        <Button
                            className={styles.login}
                            type="primary"
                            onClick={() => signIn()}
                        >
                            Войти
                        </Button>
                    </nav>
                    <Title className={styles.mainTitle}>ФУДШЕРИНГ</Title>
                    <Title className={styles.mainSubtitle} level={2}>рациональное использование продуктов
                        питания</Title>
                    <div className={styles.btn__wrapper}>
                        <Button className={styles.learnMore} type="primary">
                            <a
                                href="https://foodsharing.ru/"
                                rel="noreferrer"
                                target='_blank'
                            >
                                Узнать больше о проекте
                            </a>
                        </Button>
                        <div/>
                    </div>
                </div>
            </div>

            <section>
                <div className={styles.section__container}>
                    <div className={styles.section__item}>
                        <Title className={styles.section__title} level={3}>6</Title>
                        <Title level={4} className={styles.section__subtitle}>лет в России</Title>
                    </div>
                    <div className={styles.section__item}>
                        <Title level={3} className={styles.section__title}>3000+</Title>
                        <Title level={4} className={styles.section__subtitle}>волонтеров в команде</Title>
                    </div>
                    <div className={styles.section__item}>
                        <Title level={3} className={styles.section__title}>100+</Title>
                        <Title level={4} className={styles.section__subtitle}>тонн спасенных продуктов</Title>
                    </div>
                </div>
                <div className={styles.btn__wrapper}>
                    <Button
                        onClick={() => loader.start()}
                        className={styles.joinBtn}
                    >
                        <Link href={'/request'}>
                            Присоедениться к команде волонтеров
                        </Link>
                    </Button>
                </div>
            </section>
        </div>
    );
};

export default Recruting;