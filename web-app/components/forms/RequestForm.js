import {Form, Button, Typography} from "antd";
import {REQUEST_FORM_INPUT} from "../../consts/requestForm";
import styles from '../../styles/components/forms/RequestForm.module.css'
import axios from "axios";
import {BACKEND_ACCOUNT_REGISTER} from "../../consts/server";
import {useState} from "react";
import ApiError from "../layout/ApiError";
import ApiSuccess from "../layout/ApiSuccess";

const RequestForm = ({loader}) => {
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)

    if(error) {
        return (
            <ApiError />
        )
    }
    if(success) {
        return (
            <ApiSuccess />
        )
    }

    const register = async (body) => {
        try {
            loader.start()
            await axios.post(BACKEND_ACCOUNT_REGISTER, body)
            setSuccess(true)
            loader.end()
        } catch (err) {
            setError(true)
            loader.end()
        }
    }

    return (
        <div className="container-page">
            <Form
                layout={'vertical'}
                className={styles.form}
                name="requestForm"
                onFinish={register}
            >
                <Typography.Title style={{textAlign: 'center'}} level={3}>Заявка в АНО&nbsp;&quot;ФУДШЕРИНГ&quot;</Typography.Title>
                {REQUEST_FORM_INPUT.map(item => {
                    return <Form.Item
                        key={item.name}
                        name={item.name}
                        label={item.label}
                        rules={item.rules}
                        style={item.style}
                    >
                        {item.children}
                    </Form.Item>
                })}

                <Form.Item
                    className={styles.submit}
                    key='submit'
                >
                    <Button
                        style={{width: '100%'}}
                        type="primary"
                        htmlType="submit"
                    >
                        Стать волонтером!
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default RequestForm;