import {Button} from "antd";
import {ACTIVE_USER} from "../../consts/statesUsers";
import {useState} from "react";
import ApiError from "../layout/ApiError";
import axios from "axios";
import {BACKEND_USER_CHANGE_STATE} from "../../consts/server";

const BlockUser = ({user, auth, loader, postBlock}) => {
    const [error, setError] = useState(false)

    if (error) {
        return (
            <ApiError/>
        )
    }

    const action = async (state) => {
        try {
            loader.start()
            await axios.post(BACKEND_USER_CHANGE_STATE, {userId: user.id, state: state}, {headers: {...auth}})
            if (postBlock) {
                await postBlock()
            }
            loader.end()
        } catch {
            setError(true)
            loader.end()
        }
    }

    if (user.state === ACTIVE_USER) {
        return (
            <Button onClick={() => action(3)} size={'small'} danger>Заблокировать</Button>
        );
    }

    return (
        <Button onClick={() => action(2)} size={'small'}>Разблокировать</Button>
    );
};

export default BlockUser;