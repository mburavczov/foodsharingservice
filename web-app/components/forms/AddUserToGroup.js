import {Button, Form, Input, Select, Typography} from "antd";

const AddUserToGroup = ({users, userAddToGroup, groupId}) => {
    const [form] = Form.useForm()

    if (users.length === 0) {
        return (
            <Typography.Title level={5} style={{marginBottom: '10px'}}>Нет свободных волонтеров, доступных к
                добавлению.</Typography.Title>
        )
    }

    return (
        <Form
            form={form}
            layout={'vertical'}
            onFinish={(body) => userAddToGroup(body, form)}
            style={{width: '200px'}}
        >
            <Typography.Title level={5} style={{marginBottom: '10px'}}>Добавление волонтера</Typography.Title>
            <Form.Item
                name={'groupId'}
                initialValue={groupId}
                hidden={true}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                name={'userId'}
                rules={[{required: true, message: ''}]}
            >
                <Select
                    placeholder="Выбрать участника"
                >
                    {
                        users.map(item => <Select.Option key={item.id}
                                                         value={item.id}>{`${item.name} ${item.surname}`}</Select.Option>)
                    }
                </Select>
            </Form.Item>
            <Form.Item style={{marginTop: '-10px'}}>
                <Button
                    style={{width: '100%'}}
                    type="primary"
                    htmlType="submit"
                >
                    Добавить
                </Button>
            </Form.Item>
        </Form>
    );
};

export default AddUserToGroup;