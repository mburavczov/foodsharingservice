import {Button, Form, Input, Typography} from "antd";
import styles from '../../styles/components/forms/AuthForm.module.css'

const AuthForm = ({csrfToken, onFinish, error}) => {
    return (
        <Form
            name="auth"
            layout={'vertical'}
            onFinish={onFinish}
        >
            <Typography.Title level={3} className={styles.title}>АНО&nbsp;&quot;ФУДШЕРИНГ&quot;</Typography.Title>
            <Typography.Title level={5} type="danger" className={styles.title}>{(error ? 'Неверный логин или пароль' : '')}</Typography.Title>
            <Form.Item
                name={'csrfToken'}
                initialValue={csrfToken}
                hidden={true}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="E-mail"
                name="login"
                rules={[
                    {
                        required: true,
                        message: 'Данной поле должно быть заполнено'
                    },
                    {
                        type: 'email',
                        message: 'Введите корректный e-mail'
                    }
                ]}
            >
                <Input placeholder={'Введите e-mail'}/>
            </Form.Item>
            <Form.Item
                label="Пароль"
                name="pass"
                rules={[
                    {
                        required: true,
                        message: 'Необходимо ввести пароль'
                    },
                ]}
            >
                <Input.Password placeholder={'Введите пароль'}/>
            </Form.Item>
            <Form.Item
                key='submit'
            >
                <Button
                    style={{width: '100%'}}
                    type="primary"
                    htmlType="submit"
                >
                    Войти
                </Button>
            </Form.Item>
        </Form>
    );
};

export default AuthForm;