import {Upload, Button, Typography} from 'antd';
import {UploadOutlined} from '@ant-design/icons';
import {useState} from "react";
import axios from "axios";
import {BACKEND_FILE_UPLOAD} from "../../consts/server";
import ApiError from "../layout/ApiError";

const initState = {
    fileList: [],
    uploading: false,
}

const AddActForm = ({userId, auth, loader}) => {
    const [state, setState] = useState(initState)
    const [message, setMessage] = useState(null)
    const [error, setError] = useState(false)

    if(error) {
        return (
            <ApiError />
        )
    }

    const handleUpload = async () => {
        const formData = new FormData()
        state.fileList.forEach(file => {
            formData.append('files[]', file);
        })
        setState(prev => ({...prev, uploading: true}))
        formData.append('UserId', userId)
        try {
            loader.start()
            await axios.post(BACKEND_FILE_UPLOAD, formData, {headers: {...auth}})
            setState(initState)
            setMessage('Отправка данных прошла успешно')
            loader.end()
        } catch {
            setError(true)
            loader.end()
        }
    }

    const props = {
        beforeUpload: file => {
            setState(state => ({
                fileList: [...state.fileList, file],
            }));
            return false;
        },
        fileList: state.fileList
    };

    return (
        <>
            <Typography.Paragraph type="success">{message}</Typography.Paragraph>
            <Upload {...props}>
                <Button icon={<UploadOutlined/>}>Загрузите файл</Button>
            </Upload>
            <Button
                type="primary"
                onClick={handleUpload}
                disabled={state.fileList.length === 0}
                loading={state.uploading}
                style={{marginTop: 16}}
            >
                {'Отправить'}
            </Button>
        </>
    );
};

export default AddActForm;