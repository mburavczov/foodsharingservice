import {Button, Form, Input, Result, Select} from "antd";

const GroupCreateForm = ({supervisors, groupAdd}) => {
    if(!supervisors) {
        return (
            <Result
                status="error"
                title="Нет свободных координаторов."
            >
            </Result>
        )
    }

    return (
        <Form
            layout={'vertical'}
            onFinish={groupAdd}
        >
            <Form.Item
                name={'name'}
                rules={[{ required: true, message: '' }]}
                label={'Название группы'}
            >
                <Input placeholder={'Введите название группы'}/>
            </Form.Item>
            <Form.Item
                name={'description'}
                label={'Описание группы'}
            >
                <Input.TextArea autoSize={true} placeholder={'Введите описание группы'}/>
            </Form.Item>
            <Form.Item
                name={'supervisorId'}
                rules={[{ required: true, message: '' }]}
                label={'Координатор группы'}
            >
                <Select
                    placeholder="Выберите координатора"
                >
                    {
                        supervisors.map(item => <Select.Option key={item.id} value={item.id}>{`${item.name} ${item.surname}`}</Select.Option>)
                    }
                </Select>
            </Form.Item>
            <Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                >
                    Создать
                </Button>
            </Form.Item>
        </Form>
    );
};

export default GroupCreateForm;