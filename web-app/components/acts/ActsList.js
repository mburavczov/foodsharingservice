import {CaretRightOutlined} from "@ant-design/icons";
import {Collapse} from "antd";
import ActTable from "./ActTable";

const ActsList = ({acts}) => {
    if(acts.length === 0) {
        return (
            <>
                <h1>На данный момент нет ни одного акта.</h1>
            </>
        )
    }

    return (
        <>
            <h1>Список актов</h1>
            <Collapse
                style={{marginTop: '10px'}}
                bordered={false}
                expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                className="site-collapse-custom-collapse"
            >
                {acts.map(item => {
                    return (
                        <Collapse.Panel
                            header={`Акт от ${new Date(item.createdAt).toLocaleString()} (${item.user.name} ${item.user.surname})`}
                            key={item.id}
                            className="site-collapse-custom-panel"
                        >
                            <ActTable act={item}/>
                        </Collapse.Panel>
                    )
                })}
            </Collapse>
        </>
    );
};

export default ActsList;