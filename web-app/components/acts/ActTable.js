import {Table} from "antd";
import {COLUMNS_TABLE_ACT} from "../../consts/columnsTableAct";

const ActTable = ({act}) => {
    const dataSource = act.acts.map((item, index) => { return {
        key: item.id,
        number: index + 1,
        name: item.name,
        count: item.count,
        price: item.price,
        expire: new Date(item.expire).toLocaleDateString(),
        comment: item.comment
    }})

    return (
        <Table
            scroll={{x: true}}
            dataSource={dataSource}
            columns={COLUMNS_TABLE_ACT}
            pagination={false}
        />
    );
};

export default ActTable;