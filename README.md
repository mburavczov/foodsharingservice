# FoodSharingService

Cервис для волонтеров и координаторов фудшеринга, котрый позволит оптимизировать 
их ежедневную работу. Сервис позволяет создавать пользователей разных уровней
(админ, координатор, волонтёр), создавать группы, управлять расписание группы
и каждого волонтёра, загружать и анализировать акты приёма-передачи.

Ключевые особенности:
- Распознавание данных с фото актов, сохранение в базу, формирование статистики
- Отправка и приём заявок, разграничение прав доступа на 
волонтёра, куратора и админа
- Просмотр расписания группы, управление своим расписанием
- Блокировка пользователей, повышение прав
- Дизайн адаптирован под десктопную и мобильную версию


## Стек технологий

Бэкенд написан на [ASP.NET Core 5](https://dotnet.microsoft.com/apps/aspnet).
Фронтенд на [React](https://reactjs.org/) с использованием фреймворков 
[Next.js](https://nextjs.org/) и [Ant Design](https://ant.design/).
Для распознования изображений временно используется 
[Nanonets API](https://nanonets.com/).  


API и web-сервис запускаются внутри [Docker](https://www.docker.com/)
контейнера и разворачиваются на [Heroku](https://www.heroku.com/). Для автодеплоя
используютя pipeline'ы GitLab.


## Демо

API: https://foodsharing-api.herokuapp.com/swagger/index.html  
WEB: https://foodsharing-web.herokuapp.com/

Данные для входа (логин:пароль):
- Админ: admin@gmail.com:admin
- Куратор: user_2@gmail.com:user
- Волонтёр: ivaniavn@gmail.com:2$9OZuwh


## Развёртывание

Сервисы запускаются внутри докер контейнеров.
Команды нужно выпонять в корневой директории проекта.

Запуск API
```bash
docker build -f api/Dockerfile -t foodsharing_api .
docker run -p 8080:8080 -e "PORT=8080" -it foodsharing_api
```
Проверить запуск можно открыв swagger http://localhost:8080/swagger/index.html

Запуск web приложения
```bash
docker build -f web-app/Dockerfile -t foodsharing_web .
docker run -p 8081:8081 -e "NEXTAUTH_URL='localhost:8081'" -e "PORT=8081" -it foodsharing_web
```
Приложение будет доступно по адресу http://localhost:8081/
