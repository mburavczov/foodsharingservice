using FoodsharingApi.ApiModels.Request.Donor;
using FoodsharingApi.Entity;
using FoodsharingApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DonorController : Controller
    {
        private readonly DonorRepository _donorRepository;

        public DonorController(DonorRepository donorRepository)
        {
            _donorRepository = donorRepository;
        }
        
        
        [Authorize(Roles = "admin")]
        [HttpPost("Add")]
        public IActionResult Add(AddDonorDto data)
        {
            _donorRepository.Add(new Donor
            {
                Name = data.Name,
                Address = data.Address
            });

            return Ok();
        }
    }
}