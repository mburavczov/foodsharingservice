﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using FoodsharingApi.ApiModels.Request;
using FoodsharingApi.ApiModels.Response;
using FoodsharingApi.Entity;
using FoodsharingApi.Repositories;
using FoodsharingApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly PersonRepository _personRepository;

        public AccountController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [HttpPost("token")]
        public IActionResult Token([FromBody] AuthData data)
        {
            var person = _personRepository.GetByLoginAndPassword(data.Login, data.Pass);
            if (person == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var claims = new List<Claim>
            {
                new Claim("name", person.Name),
                new Claim("groupId", person.GroupId.ToString()),
                new Claim("role", person.Role.Name()),
                new Claim("login", person.Login),
                new Claim("user_id", person.Id.ToString())
            };
            claims.AddRange(GetIdentity(person).Claims);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = person.Name
            };

            return Json(response);
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] PersonDto data)
        {
            var pass = GenerateRandomPassword();
            var person = new Person
            {
                Login = data.Login,
                Password = pass,
                Name = data.Name,
                Surname = data.Surname,
                City = data.City,
                Address = data.Address,
                Vk = data.Vk,
                Phone = data.Phone,
                Comment = data.Comment,
                HasCar = data.HasCar,
                Role = PersonRole.User,
                State = PersonState.Pending
            };
            _personRepository.Add(person);

            return Ok(pass);
        }

        [Authorize(Roles = "admin")]
        [HttpPost("ActivateUser")]
        public IActionResult ActivateUser(ActivateUserDto data)
        {
            _personRepository.SetPersonState(data.UserId, PersonState.Active);

            return Ok();
        }

        [Authorize(Roles = "admin,supervisor")]
        [HttpGet("Requests")]
        public IActionResult Requests()
        {
            return Ok(_personRepository.PendingPersons().Select(y => new UserItem
            {
                Id = y.Id,
                Login = y.Login,
                Name = y.Name,
                Surname = y.Surname,
                City = y.City,
                Address = y.Address,
                Vk = y.Vk,
                Phone = y.Phone,
                Comment = y.Comment,
                HasCar = y.HasCar,
                Role = y.Role.Name(),
                State = y.State.Name(),
                GroupId = y.GroupId,
            }).ToList());
        }

        /// <summary>
        /// Generates a Random Password
        /// respecting the given strength requirements.
        /// </summary>
        /// <param name="opts">A valid PasswordOptions object
        /// containing the password strength requirements.</param>
        /// <returns>A random password</returns>
        private static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null)
                opts = new PasswordOptions()
                {
                    RequiredLength = 8,
                    RequiredUniqueChars = 4,
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireNonAlphanumeric = false,
                    RequireUppercase = false
                };

            string[] randomChars = new[]
            {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ", // uppercase 
                "abcdefghijkmnopqrstuvwxyz", // lowercase
                "0123456789", // digits
                "!@$?_-" // non-alphanumeric
            };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count;
                i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars;
                i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
        
        private ClaimsIdentity GetIdentity(Person person)
        {
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role.Name())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }

    }
}