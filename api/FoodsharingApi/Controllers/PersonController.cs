﻿using FoodsharingApi.ApiModels.Request.Person;
using FoodsharingApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonController : Controller
    {
        private readonly PersonRepository _personRepository;

        public PersonController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }


        [Authorize(Roles = "admin,supervisor")]
        [HttpGet("Find")]
        public IActionResult Find([FromQuery] GetUsersDto data)
        {
            return Ok(_personRepository.FindByFilter(data.Role, data.HasGroup));
        }

        [Authorize(Roles = "admin")]
        [HttpPost("ChangeRole")]
        public IActionResult ChangeRole(ChangeRoleDto data)
        {
            _personRepository.SetPersonRole(data.UserId, data.Role);

            return Ok();
        }

        [Authorize(Roles = "admin,supervisor")]
        [HttpPost("ChangeState")]
        public IActionResult ChangeState(ChangeStateDto data)
        {
            _personRepository.SetPersonState(data.UserId, data.State);

            return Ok();
        }
    }
}