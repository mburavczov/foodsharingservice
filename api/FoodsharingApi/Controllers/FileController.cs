﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FoodsharingApi.ApiModels.Request.File;
using FoodsharingApi.Entity;
using FoodsharingApi.Ocr;
using FoodsharingApi.Repositories;
using Dropbox.Api;
using Dropbox.Api.Files;
using FoodsharingApi.ApiModels.Response.File;
using FoodsharingApi.Mappers;
using FoodsharingApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using File = FoodsharingApi.Entity.File;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private readonly Regex _dateRegex = new Regex(@"\d{2}\.\d{2}\.\d{4}");
        
        private readonly FileRepository _fileRepository;
        private readonly ActDataRepository _actDataRepository;
        private readonly OcrService _ocrService;
        private readonly AppSettings _appSettings;
        private readonly ILogger<FileController> _logger;

        public FileController(FileRepository fileRepository, IOptionsSnapshot<AppSettings> snapshot, OcrService ocrService, ActDataRepository actDataRepository, ILogger<FileController> logger)
        {
            _fileRepository = fileRepository;
            _ocrService = ocrService;
            _actDataRepository = actDataRepository;
            _logger = logger;
            _appSettings = snapshot.Value;
        }


        [HttpPost("Upload")]
        public async Task<IActionResult> Upload([FromForm] UploadDto data)
        {
            var fileId = Guid.NewGuid();
            using (var dbx = new DropboxClient(_appSettings.DropboxToken))
            {
                foreach (var f in Request.Form.Files)
                {
                    using (var s = f.OpenReadStream())
                    {
                        var bytes = ReadFully(s);
                        var updated = await dbx.Files.UploadAsync(
                            $"/{data.UserId}/{fileId}.jpg",
                            WriteMode.Overwrite.Instance,
                            body: new MemoryStream(bytes));
                        _fileRepository.Add(new File
                        {
                            Id = fileId,
                            UserId = data.UserId,
                            Parsed = false
                        });

                        await ProcessFile(fileId, bytes);
                    }
                }
            }
            
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("GetAllFiles")]
        public IActionResult GetFiles()
        {
            var res = _fileRepository.GetAll().Select(x => new FileItem
            {
                Id = x.Id,
                UserId = x.UserId,
                PointId = x.PointId,
                Parsed = x.Parsed,
                CreatedAt = x.CreatedAt,
                UpdatedAt = x.UpdatedAt,
                User = UserMapper.MapUser(x.User),
                Acts = x.ActData.Select(ActMapper.MapAct).ToList()
            });

            return Ok(res);
        }

        [Authorize]
        [HttpGet("GetFiles")]
        public IActionResult GetFiles([FromQuery] int groupId)
        {
            var res = _fileRepository.GetByGroup(groupId).Select(x => new FileItem
            {
                Id = x.Id,
                UserId = x.UserId,
                PointId = x.PointId,
                Parsed = x.Parsed,
                CreatedAt = x.CreatedAt,
                UpdatedAt = x.UpdatedAt,
                User = UserMapper.MapUser(x.User),
                Acts = x.ActData.Select(ActMapper.MapAct).ToList()
            });

            return Ok(res);
        }

        [Authorize]
        [HttpGet("GetFileUrl")]
        public async Task<IActionResult> GetFileUrl([FromQuery] Guid fileId)
        {
            var file = _fileRepository.Get(fileId);
            if (file == null)
            {
                return NotFound();
            }
            
            using (var dbx = new DropboxClient(_appSettings.DropboxToken))
            {
                var path = await dbx.Files.GetTemporaryLinkAsync($"/{file.UserId}/{fileId}.jpg");
                return Ok(new FileLink
                {
                    Link = path.Link
                });
            }
        }
        
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        private async Task ProcessFile(Guid fileId, byte[] image)
        {
            try
            {
                var ocrResp = await _ocrService.ParseTable(image);
                // Если произойдёт ошибка при парсинге, файл всё равно считается обработанным,
                // а данные исправлять придётся вручную
                _fileRepository.SetParsed(fileId);
                if (ocrResp.Result?.Count > 0)
                {
                    var res = ocrResp.Result.FirstOrDefault(x => x.Message.ToLower() == "success");
                    if (res?.Prediction?.Count > 0)
                    {
                        var pred = res.Prediction.FirstOrDefault(x => x.OcrText.ToLower() == "table");
                        if (pred?.Cells?.Count > 0)
                        {
                            try
                            {
                                ParseTable(pred.Cells, fileId);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError(e, "failed to parse table");
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "failed to process file");
                throw;
            }
        }

        private void ParseTable(List<Cell> predCells, Guid fileId)
        {
            var dataRows = predCells.GroupBy(x => x.Row);
            var res = new List<ActData>();
            foreach (var row in dataRows)
            {
                var firstColumn = row.FirstOrDefault(x => x.Col == 1 && x.Text.Length > 0 && char.IsDigit(x.Text[0]));
                if (firstColumn == null)
                {
                    continue;
                }
                
                try
                {
                    res.Add(ParseRow(row.ToList(), fileId));
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "failed to parse row");
                }
            }

            _actDataRepository.AddMany(res);
        }

        private ActData ParseRow(List<Cell> row, Guid fileId)
        {
            var data = new ActData
            {
                FileId = fileId
            };

            foreach (var cel in row)
            {
                var text = cel.Text;
                switch (cel.Col)
                {
                    case 2:
                        data.Name = text;
                        break;
                    case 3:
                        data.Count = int.TryParse(text, NumberStyles.Any, CultureInfo.InvariantCulture, out var r) ? r : -1;
                        break;
                    case 4:
                        data.Price = decimal.TryParse(text.Replace(',', '.'), NumberStyles.Any,
                            CultureInfo.InvariantCulture, out var p)
                            ? p
                            : -1;
                        break;
                    case 5:
                        data.Expire = ParseDateTime(text);
                        break;
                    case 6:
                        data.Comment = text;
                        break;
                }
            }

            return data;
        }

        private DateTime? ParseDateTime(string s)
        {
            var date = s.Split(" ").FirstOrDefault(x => _dateRegex.IsMatch(x));
            if (!string.IsNullOrEmpty(date))
            {
                return DateTime.ParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }

            return null;
        }
    }
}