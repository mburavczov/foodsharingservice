﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using FoodsharingApi.ApiModels.Request.Group;
using FoodsharingApi.ApiModels.Response;
using FoodsharingApi.ApiModels.Response.Group;
using FoodsharingApi.Entity;
using FoodsharingApi.Exceptions;
using FoodsharingApi.Mappers;
using FoodsharingApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Donor = FoodsharingApi.Entity.Donor;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GroupController : Controller
    {
        private readonly GroupRepository _groupRepository;
        private readonly PersonRepository _personRepository;

        public GroupController(GroupRepository groupRepository, PersonRepository personRepository)
        {
            _groupRepository = groupRepository;
            _personRepository = personRepository;
        }


        [Authorize(Roles = "admin")]
        [HttpPost("Create")]
        public IActionResult Create(CreateGroupDto data)
        {
            var g = _groupRepository.Add(new Group
            {
                Name = data.Name,
                Description = data.Description,
                SupervisorId = data.SupervisorId
            });
            
            _personRepository.AddToGroup(data.SupervisorId, g.Id);

            return Ok();
        }

        [Authorize(Roles = "admin,supervisor")]
        [HttpPost("AddUser")]
        public IActionResult AddUser(AddUserDto data)
        {
            try
            {
                _personRepository.AddToGroup(data.UserId, data.GroupId);
            }
            catch (NotFoundException e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("Groups")]
        public IActionResult Groups()
        {
            return Ok(MapGroups(_groupRepository.GetAll()));
        }

        [Authorize]
        [HttpGet("Groups/{id:int}")]
        public IActionResult Group(int id)
        {
            Group group;
            if (User.HasClaim(ClaimsIdentity.DefaultRoleClaimType, PersonRole.Admin.Name()))
            {
                group = _groupRepository.Get(id);
            }
            else
            {
                var userId = int.Parse(User.Claims.First(x => x.Type == "user_id").Value);
                group = _groupRepository.GetByUser(id, userId);
            }
            if (group == null)
            {
                return NotFound();
            }
            
            return Ok(MapGroup(group));
            
        }

        private List<GroupItem> MapGroups(List<Group> groups)
        {
            return groups.Select(MapGroup).ToList();
        }

        private GroupItem MapGroup(Group x)
        {
            return new GroupItem
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                SupervisorId = x.Supervisor?.Id ?? 0,
                SupervisorName = $"{x.Supervisor?.Name} {x.Supervisor?.Surname}",
                Users = x.Users?.Select(UserMapper.MapUser).ToList()
            };
        }
    }
}