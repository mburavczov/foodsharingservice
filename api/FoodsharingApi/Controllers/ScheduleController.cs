﻿using System.Linq;
using FoodsharingApi.ApiModels.Request.Schedule;
using FoodsharingApi.ApiModels.Response.Schedule;
using FoodsharingApi.Entity;
using FoodsharingApi.Mappers;
using FoodsharingApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodsharingApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        private readonly ScheduleRepository _scheduleRepository;

        
        public ScheduleController(ScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }

        
        [Authorize]
        [HttpPost("Add")]
        public IActionResult Add(AddScheduleDto data)
        {
            if (!data.Date.HasValue && !data.DayOfWeek.HasValue)
            {
                return BadRequest("Invalid schedule");
            }
            
            _scheduleRepository.Add(new Schedule
            {
                UserId = data.UserId,
                Date = data.Date,
                DayOfWeek = data.DayOfWeek,
                Type = data.Type
            });

            return Ok();
        }

        [Authorize]
        [HttpPost("Delete")]
        public IActionResult Delete(DeleteScheduleDto data)
        {
            _scheduleRepository.Delete(data.Id);

            return Ok();
        }

        [Authorize]
        [HttpGet("GetForGroup")]
        public IActionResult GetForGroup([FromQuery] int groupId)
        {
            return Ok(_scheduleRepository.GetForGroup(groupId).Select(x => new ScheduleItem
            {
                Id = x.Id,
                UserId = x.UserId,
                DayOfWeek = x.DayOfWeek,
                Date = x.Date,
                Type = x.Type,
                User = UserMapper.MapUser(x.User)
            }).ToList());
        }
    }
}