namespace FoodsharingApi.ApiModels.Request.Donor
{
    public class AddDonorDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}