﻿namespace FoodsharingApi.ApiModels.Request
{
    public class AuthData
    {
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}