﻿namespace FoodsharingApi.ApiModels.Request.Group
{
    public class AddUserDto
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}