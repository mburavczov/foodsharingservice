﻿namespace FoodsharingApi.ApiModels.Request.Group
{
    public class CreateGroupDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int SupervisorId { get; set; }
    }
}