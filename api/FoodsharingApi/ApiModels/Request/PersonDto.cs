﻿namespace FoodsharingApi.ApiModels.Request
{
    public class PersonDto
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Vk { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public bool HasCar { get; set; }
    }
}