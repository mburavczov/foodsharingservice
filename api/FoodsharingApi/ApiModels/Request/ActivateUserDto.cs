﻿namespace FoodsharingApi.ApiModels.Request
{
    public class ActivateUserDto
    {
        public int UserId { get; set; }
    }
}