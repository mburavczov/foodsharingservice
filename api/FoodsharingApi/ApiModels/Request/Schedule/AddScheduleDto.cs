﻿using System;
using FoodsharingApi.Entity;

namespace FoodsharingApi.ApiModels.Request.Schedule
{
    public class AddScheduleDto
    {
        public int UserId { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public DateTime? Date { get; set; }
        public ScheduleType Type { get; set; }
    }
}