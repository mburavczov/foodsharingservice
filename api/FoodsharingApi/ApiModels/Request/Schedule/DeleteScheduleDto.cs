﻿namespace FoodsharingApi.ApiModels.Request.Schedule
{
    public class DeleteScheduleDto
    {
        public int Id { get; set; }
    }
}