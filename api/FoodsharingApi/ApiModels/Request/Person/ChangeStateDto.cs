﻿using FoodsharingApi.Entity;

namespace FoodsharingApi.ApiModels.Request.Person
{
    public class ChangeStateDto
    {
        public int UserId { get; set; }
        public PersonState State { get; set; }
    }
}