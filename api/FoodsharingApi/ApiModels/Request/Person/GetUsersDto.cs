﻿using FoodsharingApi.Entity;

namespace FoodsharingApi.ApiModels.Request.Person
{
    public class GetUsersDto
    {
        public PersonRole Role { get; set; }
        public bool HasGroup { get; set; }
    }
}