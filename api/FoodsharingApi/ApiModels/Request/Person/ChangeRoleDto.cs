﻿using FoodsharingApi.Entity;

namespace FoodsharingApi.ApiModels.Request.Person
{
    public class ChangeRoleDto
    {
        public int UserId { get; set; }
        public PersonRole Role { get; set; }
    }
}