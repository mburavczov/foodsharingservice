﻿using System;
using FoodsharingApi.Entity;

namespace FoodsharingApi.ApiModels.Response.Schedule
{
    public class ScheduleItem
    {
        public int Id { get; set; }
        
        public int UserId { get; set; }
        
        public DayOfWeek? DayOfWeek { get; set; }
        
        public DateTime? Date { get; set; }
        
        public ScheduleType Type { get; set; }
        
        public UserItem User { get; set; }
    }
}