﻿using System.Collections.Generic;

namespace FoodsharingApi.ApiModels.Response.Group
{
    public class GroupItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public List<UserItem> Users { get; set; }
    }
}