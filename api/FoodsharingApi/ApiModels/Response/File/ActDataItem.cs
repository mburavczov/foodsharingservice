using System;

namespace FoodsharingApi.ApiModels.Response.File
{
    public class ActDataItem
    {
        public int Id { get; set; }
        public Guid FileId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public DateTime? Expire { get; set; }
        public string Comment { get; set; }
    }
}