namespace FoodsharingApi.ApiModels.Response.File
{
    public class FileLink
    {
        public string Link { get; set; }
    }
}