using System;
using System.Collections.Generic;

namespace FoodsharingApi.ApiModels.Response.File
{
    public class FileItem
    {
        public Guid Id { get; set; }
        public int UserId { get; set; }
        public int PointId { get; set; }
        public bool Parsed { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public UserItem User { get; set; }
        public List<ActDataItem> Acts { get; set; }
    }
}