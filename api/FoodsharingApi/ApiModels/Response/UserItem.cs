namespace FoodsharingApi.ApiModels.Response
{
    public class UserItem
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Vk { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public bool HasCar { get; set; }
        public string Role { get; set; }
        public string State { get; set; }
        public int? GroupId { get; set; }
        public Donor Donor { get; set; }
    }
}