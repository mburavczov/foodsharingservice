using FoodsharingApi.ApiModels.Response;
using FoodsharingApi.Entity;

namespace FoodsharingApi.Mappers
{
    public static class UserMapper
    {
        public static UserItem MapUser(Person y)
        {
            return new UserItem
            {
                Id = y.Id,
                Login = y.Login,
                Name = y.Name,
                Surname = y.Surname,
                City = y.City,
                Address = y.Address,
                Vk = y.Vk,
                Phone = y.Phone,
                Comment = y.Comment,
                HasCar = y.HasCar,
                Role = y.Role.Name(),
                State = y.State.Name(),
                GroupId = y.GroupId,
                Donor = DonorMapper.MapDonor(y.Donor)
            };
        }
    }
}