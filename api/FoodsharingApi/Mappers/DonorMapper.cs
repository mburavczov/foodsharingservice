using FoodsharingApi.Entity;

namespace FoodsharingApi.Mappers
{
    public static class DonorMapper
    {
        public static ApiModels.Response.Donor MapDonor(Donor donor)
        {
            if (donor != null)
            {
                return new ApiModels.Response.Donor
                {
                    Id = donor.Id,
                    Name = donor.Name,
                    Address = donor.Address
                };
            }

            return null;
        }
    }
}