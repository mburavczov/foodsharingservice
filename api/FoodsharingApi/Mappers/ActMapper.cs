using FoodsharingApi.ApiModels.Response.File;
using FoodsharingApi.Entity;

namespace FoodsharingApi.Mappers
{
    public static class ActMapper
    {
        public static ActDataItem MapAct(ActData x)
        {
            return new ActDataItem
            {
                Id = x.Id,
                FileId = x.FileId,
                Name = x.Name,
                Count = x.Count,
                Price = x.Price,
                Expire = x.Expire,
                Comment = x.Comment
            };
        }
    }
}