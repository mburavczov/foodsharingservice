﻿namespace FoodsharingApi.Entity
{
    public enum PersonState
    {
        Pending = 1,
        Active = 2,
        Disabled = 3
    }
    
    public static class PersonStateExtension
    {
        public static string Name(this PersonState personState)
        {
            return personState switch
            {
                PersonState.Pending => "pending",
                PersonState.Active => "active",
                PersonState.Disabled => "disabled",
                _ => ""
            };
        }
    }
}