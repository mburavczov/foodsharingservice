﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodsharingApi.Entity
{
    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Vk { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        [Required]
        public bool HasCar { get; set; }
        [Required]
        public PersonRole Role { get; set; }
        public PersonState State { get; set; }
        public int? GroupId { get; set; }
        public int? DonorId { get; set; }
        
        public virtual Group Group { get; set; }
        public virtual Donor Donor { get; set; }
        public virtual Collection<Schedule> Schedules { get; set; }
    }
}