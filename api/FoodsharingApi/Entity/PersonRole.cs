﻿namespace FoodsharingApi.Entity
{
    public enum PersonRole
    {
        Admin = 1,
        Supervisor = 2,
        User = 3
    }

    public static class RoleExtension
    {
        public static string Name(this PersonRole personRole)
        {
            return personRole switch
            {
                PersonRole.Admin => "admin",
                PersonRole.Supervisor => "supervisor",
                PersonRole.User => "user",
                _ => ""
            };
        }
    }
}