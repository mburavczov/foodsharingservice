﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace FoodsharingApi.Entity
{
    public class Group
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public int SupervisorId { get; set; }

        public virtual Collection<Person> Users { get; set; }
        
        [NotMapped]
        public Person Supervisor => Users?.FirstOrDefault(x => x.Id == SupervisorId);
    }
}