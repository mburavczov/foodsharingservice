﻿using FoodsharingApi.Entity.Configuration;
using Microsoft.EntityFrameworkCore;

namespace FoodsharingApi.Entity
{
    public class MainContext : DbContext
    {
        public MainContext()
        {
            Database.EnsureCreated();
        }
 
        public MainContext(DbContextOptions options) : base(options) {}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseNpgsql("Host=ec2-52-208-229-228.eu-west-1.compute.amazonaws.com;Port=5432;Database=dds3qq72vsqle6;Username=ykmwgckbjpqjmv;Password=56d7824568e841990c3231e3a7494468a2e1c35d6a0ecf83b72a5e129a6d1780;Trust Server Certificate=true;SslMode=Require;");
            // optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=foodhunter;Username=postgres;Password=admin;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PersonConfiguration());
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Group> Groups { get; set; }
    }
}