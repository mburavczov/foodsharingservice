﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodsharingApi.Entity.Configuration
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasIndex(x => x.Login).IsUnique();
            builder.HasOne(x => x.Group)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.GroupId);
            builder.HasMany<Schedule>()
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);
            builder.HasOne<Donor>()
                .WithOne(x => x.User)
                .HasForeignKey<Person>(x => x.DonorId);
            builder.HasMany<File>()
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);
        }
    }
}