﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodsharingApi.Entity.Configuration
{
    public class FileConfiguration : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.HasMany<ActData>()
                .WithOne(x => x.File)
                .HasForeignKey(x => x.FileId);
        }
    }
}