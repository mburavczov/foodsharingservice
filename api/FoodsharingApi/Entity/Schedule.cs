﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodsharingApi.Entity
{
    public enum ScheduleType
    {
        Periodic = 1,
        Include = 2,
        Exclude = 3
    }
    
    public class Schedule
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        public int UserId { get; set; }
        
        public DayOfWeek? DayOfWeek { get; set; }
        
        public DateTime? Date { get; set; }
        
        [Required]
        public ScheduleType Type { get; set; }
        
        
        public virtual Person User { get; set; }
    }
}