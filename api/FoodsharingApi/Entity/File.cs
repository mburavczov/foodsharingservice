﻿using System;
using System.Collections.Generic;

namespace FoodsharingApi.Entity
{
    public class File
    {
        public Guid Id { get; set; }
        public int UserId { get; set; }
        public int PointId { get; set; }
        public bool Parsed { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; }
        
        public virtual Person User { get; set; }
        public virtual List<ActData> ActData { get; set; }
    }
}