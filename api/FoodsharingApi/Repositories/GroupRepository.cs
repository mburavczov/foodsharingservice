﻿using System.Collections.Generic;
using System.Linq;
using FoodsharingApi.Entity;
using Microsoft.EntityFrameworkCore;

namespace FoodsharingApi.Repositories
{
    public class GroupRepository : BaseRepository<Group>
    {
        public GroupRepository(MainContext context) : base(context)
        {
        }

        public List<Group> GetAll()
        {
            return DbSet.Include(x => x.Users).ToList();
        }

        public Group Get(int id)
        {
            return DbSet.Include(x => x.Users)
                .FirstOrDefault(x => x.Id == id);
        }

        public Group GetByUser(int id, int userId)
        {
            return DbSet.Include(x => x.Users)
                .FirstOrDefault(x => x.Id == id && x.Users.Any(u => u.Id == userId));
        }
    }
}