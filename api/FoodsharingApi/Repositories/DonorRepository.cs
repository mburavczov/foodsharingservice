using FoodsharingApi.Entity;

namespace FoodsharingApi.Repositories
{
    public class DonorRepository : BaseRepository<Donor>
    {
        public DonorRepository(MainContext context) : base(context)
        {
        }
    }
}