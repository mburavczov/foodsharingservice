﻿using System.Collections.Generic;
using System.Linq;
using FoodsharingApi.Entity;
using FoodsharingApi.Exceptions;

namespace FoodsharingApi.Repositories
{
    public class PersonRepository : BaseRepository<Person>
    {
        public PersonRepository(MainContext context) : base(context)
        {
        }

        public Person GetByLoginAndPassword(string login, string password)
        {
            return DbSet.FirstOrDefault(x => x.State == PersonState.Active && x.Login == login && x.Password == password);
        }

        public void SetPersonState(int id, PersonState state)
        {
            var person = DbSet.FirstOrDefault(x => x.Id == id);
            if (person != null)
            {
                person.State = state;
            }

            Context.SaveChanges();
        }
        
        public void SetPersonRole(int id, PersonRole role)
        {
            var person = DbSet.FirstOrDefault(x => x.Id == id);
            if (person != null)
            {
                person.Role = role;
            }

            Context.SaveChanges();
        }

        public List<Person> PendingPersons()
        {
            return DbSet.Where(x => x.State == PersonState.Pending && x.Role == PersonRole.User).ToList();
        }

        public void AddToGroup(int userId, int groupId)
        {
            var person = DbSet.FirstOrDefault(x => x.Id == userId);
            if (person == null)
            {
                throw new NotFoundException("User not found");
            }

            person.GroupId = groupId;
            Context.SaveChanges();
        }

        public List<Person> FindByFilter(PersonRole role, bool hasGroup)
        {
            return DbSet
                .Where(x => x.Role == role && x.GroupId.HasValue == hasGroup && x.State == PersonState.Active)
                .ToList();
        }
    }
}