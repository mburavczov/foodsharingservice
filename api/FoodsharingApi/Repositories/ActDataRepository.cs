﻿using System.Collections.Generic;
using FoodsharingApi.Entity;

namespace FoodsharingApi.Repositories
{
    public class ActDataRepository : BaseRepository<ActData>
    {
        public ActDataRepository(MainContext context) : base(context)
        {
        }


        public void AddMany(IEnumerable<ActData> acts)
        {
            DbSet.AddRange(acts);
            Context.SaveChanges();
        }
    }
}