﻿using System.Collections.Generic;
using System.Linq;
using FoodsharingApi.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodsharingApi.Repositories
{
    public class ScheduleRepository : BaseRepository<Schedule>
    {
        public ScheduleRepository(MainContext context) : base(context)
        {
        }
        
        
        public void Delete(int id)
        {
            var s = DbSet.FirstOrDefault(x => x.Id == id);
            if (s != null)
            {
                DbSet.Remove(s);
            }

            Context.SaveChanges();
        }

        public List<Schedule> GetForGroup([FromQuery] int groupId)
        {
            return DbSet
                .Include(x => x.User)
                .Include(x => x.User.Group)
                .Where(x => x.User.State == PersonState.Active && x.User.GroupId == groupId)
                .ToList();
        }
    }
}