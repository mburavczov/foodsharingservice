﻿using System;
using System.Collections.Generic;
using System.Linq;
using FoodsharingApi.Entity;
using Microsoft.EntityFrameworkCore;

namespace FoodsharingApi.Repositories
{
    public class FileRepository : BaseRepository<File>
    {
        public FileRepository(MainContext context) : base(context)
        {
        }

        
        public File Get(Guid id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }
        
        public List<File> GetAll()
        {
            return DbSet.Include(x => x.User)
                .Include(x => x.ActData)
                .OrderByDescending(x => x.CreatedAt)
                .ToList();
        }

        public void SetParsed(Guid id)
        {
            var f = DbSet.FirstOrDefault(x => x.Id == id);
            if (f != null)
            {
                f.Parsed = true;
                f.UpdatedAt = DateTime.Now;
            }

            Context.SaveChanges();
        }

        public List<File> GetByGroup(int groupId)
        {
            return DbSet.Include(x => x.User)
                .Include(x => x.ActData)
                .Where(x => x.User.GroupId == groupId)
                .OrderByDescending(x => x.CreatedAt)
                .ToList();
        }
    }
}