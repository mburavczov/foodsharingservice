namespace FoodsharingApi
{
    public class EmailSettings
    {
        public string Address { get; set; }
        
        public string DisplayName { get; set; }
        
        public string Login { get; set; }
        
        public string Password { get; set; }
        
        public string SmtpServer { get; set; }
        
        public int SmtpPort { get; set; }
        
        public bool EnableSsl { get; set; }
    }
}