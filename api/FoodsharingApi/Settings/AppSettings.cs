﻿namespace FoodsharingApi.Settings
{
    public class AppSettings
    {
        public string DropboxToken { get; set; }
        public string OcrApi { get; set; }
        public string OcrKey { get; set; }
        public string OcrModelId { get; set; }
    }
}