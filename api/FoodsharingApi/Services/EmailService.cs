using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace FoodsharingApi.Services
{
    public class EmailService
    {
        private readonly EmailSettings _settings;


        public EmailService(IOptionsSnapshot<EmailSettings> settings)
        {
            _settings = settings.Value;
        }
        

        // public async Task Send(Report report)
        // {
        //     var from = new MailAddress(_settings.Address, _settings.DisplayName);
        //     var message = new MailMessage();
        //     message.From = from;
        //     foreach (var mail in report.Recipients)
        //     {
        //         message.To.Add(mail);
        //     }
        //     message.Subject = report.Title;
        //     message.Body = report.Body;
        //     message.IsBodyHtml = false;
        //     var smtp = new SmtpClient(_settings.SmtpServer, _settings.SmtpPort)
        //     {
        //         Credentials = new NetworkCredential(_settings.Login, _settings.Password), 
        //         EnableSsl = _settings.EnableSsl
        //     };
        //     
        //     await smtp.SendMailAsync(message);
        // }
    }
}