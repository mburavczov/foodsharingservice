﻿namespace FoodsharingApi.Ocr
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public partial class OcrResponse
    {
        [JsonProperty("message")] public string Message { get; set; }

        [JsonProperty("result")] public List<Result> Result { get; set; }

        [JsonProperty("signed_urls")] public SignedUrls SignedUrls { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("message")] public string Message { get; set; }

        [JsonProperty("input")] public string Input { get; set; }

        [JsonProperty("prediction")] public List<Prediction> Prediction { get; set; }

        [JsonProperty("page")] public long Page { get; set; }

        [JsonProperty("request_file_id")] public Guid RequestFileId { get; set; }

        [JsonProperty("filepath")] public string Filepath { get; set; }

        [JsonProperty("id")] public Guid Id { get; set; }

        [JsonProperty("rotation")] public long Rotation { get; set; }
    }

    public partial class Prediction
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("label")] public string Label { get; set; }

        [JsonProperty("xmin")] public long Xmin { get; set; }

        [JsonProperty("ymin")] public long Ymin { get; set; }

        [JsonProperty("xmax")] public long Xmax { get; set; }

        [JsonProperty("ymax")] public long Ymax { get; set; }

        [JsonProperty("score")] public long Score { get; set; }

        [JsonProperty("ocr_text")] public string OcrText { get; set; }

        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("cells")] public List<Cell> Cells { get; set; }
    }

    public partial class Cell
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("row")] public long Row { get; set; }

        [JsonProperty("col")] public long Col { get; set; }

        [JsonProperty("row_span")] public long RowSpan { get; set; }

        [JsonProperty("col_span")] public long ColSpan { get; set; }

        [JsonProperty("label")] public string Label { get; set; }

        [JsonProperty("xmin")] public long Xmin { get; set; }

        [JsonProperty("ymin")] public long Ymin { get; set; }

        [JsonProperty("xmax")] public long Xmax { get; set; }

        [JsonProperty("ymax")] public long Ymax { get; set; }

        [JsonProperty("score")] public double Score { get; set; }

        [JsonProperty("text")] public string Text { get; set; }

        [JsonProperty("status")] public string Status { get; set; }

        [JsonProperty("row_label")] public string RowLabel { get; set; }
    }

    public partial class SignedUrls
    {
        [JsonProperty("uploadedfiles/0ce0c110-29c1-4202-b055-611c8777438d/PredictionImages/466881092.jpeg")]
        public Uploadedfiles0Ce0C11029C14202B055611C8777438DPredictionImages466881092Jpeg
            Uploadedfiles0Ce0C11029C14202B055611C8777438DPredictionImages466881092Jpeg { get; set; }
    }

    public partial class Uploadedfiles0Ce0C11029C14202B055611C8777438DPredictionImages466881092Jpeg
    {
        [JsonProperty("original")] public Uri Original { get; set; }

        [JsonProperty("original_compressed")] public Uri OriginalCompressed { get; set; }

        [JsonProperty("thumbnail")] public Uri Thumbnail { get; set; }

        [JsonProperty("acw_rotate_90")] public Uri AcwRotate90 { get; set; }

        [JsonProperty("acw_rotate_180")] public Uri AcwRotate180 { get; set; }

        [JsonProperty("acw_rotate_270")] public Uri AcwRotate270 { get; set; }

        [JsonProperty("original_with_long_expiry")]
        public Uri OriginalWithLongExpiry { get; set; }
    }
}